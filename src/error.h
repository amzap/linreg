#ifndef _ERROR_H
#define _ERROR_H

// Call the error handler
#define ERROR(reason) \
    error(reason, __FILE__, __LINE__)

void error (const char * reason, const char * file, int line);

#endif

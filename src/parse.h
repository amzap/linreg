#ifndef _PARSE_H 
#define _PARSE_H 

#include "mat/private_matrix.h"
#include "probs.h"

#define MAX_LINE 40
/*
    File format:

    observaciones
    2008-01-25 10:00 1200 8
    2008-01-15 11:00 1150 7.5
    ...
    predicciones
    2009-10-25 23:00 4
    2009-10-20 14:00 5
    ...
    
 */
typedef enum 
{ 
    UNDEFINED,
    OBSERVATION,
    PREDICTION
} linetype_t;

/*
    parse_file.
    Populates the matrixes inside linreg_prob_t sorted by timestamp
*/
extern int parse_file(const char* filename, linreg_prob_t * problem);

#endif // _PARSE_H 

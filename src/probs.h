#ifndef _PROBS_H
#define _PROBS_H 

#include "matrix.h"

/*
    Linear Regression Problem definition:
- Observations:
    obs_timestamp: Timestamp of each observation
    IV: Matrix of independent variables (Nobs x Nvars)
    DV: Vector of dependent variables
- Predictions:
    pred_timestap: Timestamp of each prediction
    predict_X: X matrix of the predictions (Nobs x Nvars)
    predict_Y: Y matrix of the predictions. To be filled by the solver function
    
    coef: Coeficients of the linear regression. To be filled by the solver function

*/
typedef struct {
    matrix_t * obs_timestamp;
    matrix_t * IV;
    matrix_t * DV;
    matrix_t * pred_timestamp;
    matrix_t * predict_X;
    matrix_t * predict_Y;
    matrix_t * coef;
} linreg_prob_t;

// Print all the elements of the linreg_prob_t
void prob_print(linreg_prob_t * prob);

/*
    Estimate: Given a coeficient vector and an input vector, generates an estimation vector
    input: coeficient vector (column)
           input matrix: If a vector is provided, it will be expanded polynomically to match the grade (infered from the length of the coeficient vector)
           output: column vector of estimations

*/
matrix_t * estimate (const matrix_t * coef, const matrix_t * input);

/*
    Average Error.
    input: A solved problem handler
           Range of observations of which calculate the average error.
*/
double average_error(linreg_prob_t * prob, unsigned int from, unsigned int to);

/*
    Solve Linear Regression Problem.
    The problem will be solved by Least Squares method using Cholesky decomposition.
    input: A problem representation
           Grade of polynomial expansion to apply

*/
int solve_linreg(linreg_prob_t * prob, unsigned int grade);

#endif

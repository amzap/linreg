#ifndef _MATRIX_OP_H
#define _MATRIX_OP_H

#include "matrix.h"

/* 
  Matrix comparison
*/
int mat_equal(matrix_t * m, matrix_t * p);

/*
 Matrix Addition:
 Sums matrixes A + B and stores the result in matrix C

 requires
      dimensions of A and B match: A.rows == B.rows && A.cols == B.cols
      Enough space has already been allocated for C (will not malloc())
*/
void mat_sum(const matrix_t * A, const matrix_t * B, matrix_t * C);

void mat_mult_to(const matrix_t * A, const matrix_t * B, matrix_t * C);
/*
 Matrix Substraction
 Substracts matrixes A - B and stores the result in matrix C

 requires
      dimensions of A and B match: A.rows == B.rows && A.cols == B.cols
      Enough space has already been allocated for C (will not malloc())
*/
void mat_sub(const matrix_t * A, const matrix_t * B, matrix_t * C);

matrix_t * mat_mult(const matrix_t * A, const matrix_t * B);
/*
  Polinomial matrix expansion
  Creates the grade <grade> polinomial expansion of input (n x 1)
  input
        n x 1 matrix, grade
*/
matrix_t * expand_matrix(const matrix_t * input, const unsigned int grade);


#endif

#ifndef _PRIVATE_MATRIX_H
#define _PRIVATE_MATRIX_H

#include "block.h"
#include "matrix.h"

#include <stdio.h>

#define M_TRANSPOSE 1 // Matrix is transpose with respect to it's data
#define M_SUBMATRIX 2 // Matrix is a submatrix of another "master" matrix. It will access it's data

struct matrix_s
{
    size_t rows;    // Number of rows
    size_t cols;    // Number of columns
    
    block_t * block; // Pointer to the (shared?) memory block

    int flags;
    // Information to access submatrixes. We should use inheritance (C++)
    size_t sub_rows;
    size_t sub_cols;
    size_t master_cols; 
};


#endif //_PRIVATE_MATRIX_H

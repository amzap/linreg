#ifndef _MATRIX_H
#define _MATRIX_H

// Simple Matrix representation.
// Ideally we would use one of the many open-source libraries

#include "block.h"

#include <stdio.h>

/* Before having blocks, this was a good idea. Not so much now...
//#define STACK_MATRIX(r,c) (matrix_t) { .rows = r, \
//                                      .cols = c, \
//                                      .data =(TYPE*) (TYPE[r][c]) {{ (TYPE)0 }} \;
//};
*/
typedef struct matrix_s matrix_t;

/*
  Getters / setters for size and elements
*/
void mat_setsize(matrix_t * m, const size_t rows, const size_t cols);
TYPE mat_get(const matrix_t * m, const size_t i, const size_t j);
void mat_set(const matrix_t * m, const size_t i, const size_t j, TYPE value);
size_t mat_row(const matrix_t * m);
size_t mat_col(const matrix_t * m);

/* 
  Row / Column insertion. It will shift the rows columns to insert the one provided, dropping the last one 
*/
void mat_insert_row(matrix_t *  m, const matrix_t * row, const size_t index);
void mat_insert_col(matrix_t *  m, const matrix_t * col, const size_t index);

/*
  Free a matrix structure and free (or decrease refcount of) it's block
*/
void mat_free(matrix_t * m);

/* 
  Allocate a matrix structure
*/
matrix_t * mat_alloc(const size_t rows, const size_t cols);

/*
  Matrix resize
  Uses realloc to chage the size of the matrix 
*/
void mat_resize(matrix_t * m, const size_t rows, const size_t cols);

/*
  Matrix creation includes structure and block allocation
*/
matrix_t * mat_create(const size_t rows, const size_t cols);

/*
  Matrix creation from an array.
  A slightly easier way of initializing matrixes. Uses C99 features
*/
matrix_t * mat_array(const size_t rows, const size_t cols, TYPE array[rows][cols]);

/*
  Create an zero matrix of the given size. 
  It copies (TYPE)0 to each position instead of doing memset(0)
*/
matrix_t * mat_zero(const size_t rows, const size_t cols);

/* 
  Create an identity matrix of the given size. 
*/
matrix_t * mat_eye(const size_t size);

/*
  Matrix copy. 
  Creates a new matrix with the same size and copies the content
*/
matrix_t * mat_copy(const matrix_t * other); 

/*
  Matrix assignment. 
  Creates a new matrix structure and associates it with the same block as the provided matrix
  Both matrixes will end up pointing to the same data in memory: modifying one will alter the other one.
*/
matrix_t * mat_assign_to(const matrix_t * other);


/*
  Transpose matrix creation. 
  Creates a new matrix structure and new block of memory and copies the data from one matrix to another 
  in a transposed way 
*/
matrix_t * mat_transpose(const matrix_t * other);

/*
  Read Only Transpose matrix creation. 
  Creates a new matrix structure and uses the same memory as the given matrix, but changing the way the data is accessed.
  Both matrixes will end up pointing to the same data in memory: modifying one will alter the other one!
*/
matrix_t * mat_ROtranspose(const matrix_t * other);


/*
  Read Only Submmatrix creation
  Creates a new matrix structure and uses the same memory as the given matrix, but changing the way the data is accessed.
  Both matrixes will end up pointing to the same data in memory: modifying one will alter the other one!
*/
matrix_t * mat_ROsubmatrix(const matrix_t * m,
                              const size_t row_from, 
                              const size_t row_to,
                              const size_t col_from, 
                              const size_t col_to);

/*
  Read Only Submmatrix 
  Changes "sub_m" to point to the same memory as the given matrix, but changing the way the data is accessed.
  Both matrixes will end up pointing to the same data in memory: modifying one will alter the other one!
  NOTE: It will free the block originally pointed by sub_m
*/
void mat_ROsubmatrix_to(const matrix_t * m,
                              matrix_t * sub_m,
                              const size_t row_from, 
                              const size_t row_to,
                              const size_t col_from, 
                              const size_t col_to);

/* 
  Matix printing
*/
void mat_print_name(const matrix_t * m, const char * string);
void mat_print(const matrix_t * m);

#endif
